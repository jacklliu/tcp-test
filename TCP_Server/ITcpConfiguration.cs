﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace TCP_UDP
{
    public interface ITcpConfiguration
    {
        //  TCP Client连接地址
        EndPoint RemoteEndPoint { get; }

        //  TCP Client最大连接数
        int Backlog { get; }

        //  TCP Client接口缓存大小
        int ReceiveBufferSize { get; }

        //  TCP Server绑定地址
        EndPoint LocalEndPoint { get; }
    }

    public interface IReceiveEventArgs
    {
        byte[] Data { get; }
    }
    public interface IErrorEventArgs
    {
        Exception Error { get; }
    }
    class IOEventArgs : EventArgs, IErrorEventArgs, IReceiveEventArgs
    {
        public Exception Error { get; set; }
        public EndPoint LocalEndPoint { get; set; }
        public EndPoint RemoteEndPoint { get; set; }
        public byte[] Data { get; set; }
    }

    public class Configuration : ITcpConfiguration
    {
        public EndPoint RemoteEndPoint { get; set; }

        public int Backlog { get; set; }

        public int ReceiveBufferSize { get; set; }

        public EndPoint LocalEndPoint { get; set; }
    }
}
