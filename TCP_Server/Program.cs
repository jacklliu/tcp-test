﻿using Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TCP_Server
{
    class Program
    {
        static TcpServer tcpServer;

        static void Main(string[] args)
        {
            IPEndPoint local = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5577);

            IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);
            tcpServer = new TcpServer(new Configuration() { LocalEndPoint = local, RemoteEndPoint = remote, ReceiveBufferSize = 2048, MaxConnection = 5 });

            Console.WriteLine("Waiting for a client");
            string clientWrite = "";
            while (true)
            {
                clientWrite = Console.ReadLine();
                string sendStr = "Server_Send:" + clientWrite;
                if (clientWrite.ToLower() == "end")
                {
                    tcpServer.CloseSelf();
                    continue;
                }
                if (clientWrite.ToLower() == "start")
                {
                    tcpServer.StartSelf();
                    continue;
                }
                bool isSuccess = tcpServer.Send(Encoding.UTF8.GetBytes(sendStr));

                if (isSuccess)
                {
                    Console.WriteLine("发送成功。" + sendStr, ConsoleColor.Green);
                }
                else
                {
                    Console.WriteLine("发送失败。", ConsoleColor.Red);
                }
            }

            Console.ReadKey();
        }



        //static UDPServer udpServer;
        //static void Main(string[] args)
        //{
        //    IPEndPoint local = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5566);

        //    IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);
        //    udpServer = new UDPServer(new Configuration() { LocalEndPoint = local, RemoteEndPoint = remote, ReceiveBufferSize = 2048 });

        //    Console.WriteLine("Waiting for a client");
        //    string clientWrite = "";
        //    while (true)
        //    {
        //        clientWrite = Console.ReadLine();
        //        string sendStr = "Server_Send:" + clientWrite;
        //        if (clientWrite == "end")
        //        {
        //            break;
        //        }
        //        bool isSuccess = udpServer.Send(Encoding.UTF8.GetBytes(sendStr));

        //        if (isSuccess)
        //        {
        //            Console.WriteLine("发送成功。" + sendStr, ConsoleColor.Green);
        //        }
        //        else
        //        {
        //            Console.WriteLine("发送失败。", ConsoleColor.Red);
        //        }
        //    }

        //    Console.ReadKey();
        //}
    }


}
