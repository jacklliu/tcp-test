﻿using Common;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TCP_Client
{
    class Program
    {
        static Common.TcpClient tcpClient;
        static void Main(string[] args)
        {
            IPEndPoint remote = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5577);

            IPEndPoint local = new IPEndPoint(IPAddress.Any, 0);
            tcpClient = new Common.TcpClient(new Configuration() { LocalEndPoint = local, RemoteEndPoint = remote, ReceiveBufferSize = 2048 });

            string clientWrite = "";
            while (true)
            {
                clientWrite = Console.ReadLine();
                string sendStr = "Client_Send:" + clientWrite;
                if (clientWrite.ToLower() == "end")
                {
                    tcpClient.CloseSelf();
                    continue;
                }
                if (clientWrite.ToLower() == "reconnect")
                {
                    tcpClient.ReConnect();
                    continue;
                }
                bool isSuccess = tcpClient.Send(Encoding.UTF8.GetBytes(sendStr));

                if (isSuccess)
                {
                    Console.WriteLine("发送成功。" + sendStr, ConsoleColor.Green);
                }
                else
                {
                    Console.WriteLine("发送失败。", ConsoleColor.Red);
                }
            }
            Console.ReadKey();

        }



        //static Common.UDPClient udpClient;
        //static void Main(string[] args)
        //{
        //    IPEndPoint local = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5566);

        //    IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);
        //    udpClient = new Common.UDPClient(new Configuration() { LocalEndPoint = local, RemoteEndPoint = remote, ReceiveBufferSize = 2048 });
        //    string clientWrite = "";
        //    while (true)
        //    {
        //        clientWrite = Console.ReadLine();
        //        string sendStr = "Client_Send:" + clientWrite;
        //        if (clientWrite == "end")
        //        {
        //            break;
        //        }
        //        bool isSuccess = udpClient.Send(Encoding.UTF8.GetBytes(sendStr));

        //        if (isSuccess)
        //        {
        //            Console.WriteLine("发送成功。" + sendStr, ConsoleColor.Green);
        //        }
        //        else
        //        {
        //            Console.WriteLine("发送失败。", ConsoleColor.Red);
        //        }
        //    }
        //    Console.ReadKey();

        //}
    }
}
